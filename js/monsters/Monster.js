/**
 * Monster: a common monster to kill
 */
class Monster extends createjs.EventDispatcher {
    constructor(data, path, graphics) {
        super();
        
        // graphics component
        this.graphics = graphics;
        
        // path component
        this.path = path;
        this.direction = "";
        this.lastDirection = "";
        
        // data
        this.speed = data.speed;
        this.name = data.name;
        this.health = data.maxHealth;
        this.maxHealth = data.maxHealth;
        this.reward = data.reward;
        this.lifeCost = data.lifeCost;
        
        this.isDead = false;
        this.isSurvived = false;
        
        this.init();
    }
    
    /**
     * Send the monster death event
     */
    _sendDieEvent() {
        this.dispatchEvent(new createjs.Event("die"));
    }
    
    /**
     * Send the monster survival event
     */
    _sendSurviveEvent() {
        this.dispatchEvent(new createjs.Event("survive"));
    }
    
    /**
     * Init a monster
     */
    init() {
        this.path.init(this);
        this.graphics.init(this);
        
        this.graphics.on("tick", this.handleTick, this);
    }
    
    set x(value) {
        this.graphics.x = value;
    }
    
    get x() {
        return this.graphics.x;
    }
    
    set y(value) {
        this.graphics.y = value;
    }
    
    get y() {
        return this.graphics.y;
    }
    
    /**
     * Get the center position of the monster
     */
    getPosition() {
        return this.graphics.getCenterPosition(this);
    }
    
    /** 
     * Monster lose health
     */
    loseHealth(number) {
        this.health -= number;
        if (this.health <= 0) {
            this.die();
            return;
        }
        
        this.graphics.updateLife(this);
        this.graphics.sendDataChangedEvent();
    }
    
    /**
     * Monster die
     */
    die() {
        if (this.isDead)
            return;
        
        utils.data.currentLevel.gold += this.reward;
        this.health = 0;
        this.isDead = true;
        this.graphics.unselect();
        
        this._sendDieEvent();
    }
    
    /**
     * Monster survive
     */
    survive() {
        utils.data.currentLevel.lifes -= this.lifeCost;
        this.isSurvived = true;
        this.graphics.unselect();
        this._sendSurviveEvent();
    }
    
    /**
     * Callback for monster tick (move and animate the monster) 
     */
    handleTick(event) {
        if (game.paused) return;
        if (this.isDead || this.isSurvived) return;
        
        this.path.update(event, this);
        this.graphics.update(event, this);
    }
}