/**
 * GraphicsComponent
 */
class MonsterGraphicsComponent extends TDLevelSelectableContainer {
    constructor(spriteSheet) {
        super(game.levelScreen);
        this.spriteSheet = spriteSheet;
        
        this.sprite = null;
        this.healthBar = null;
    }
    
    /**
     * Get the sprite rect of the given monster
     */
    _getSpriteRect() {
        let rect = this.sprite.getBounds();
        if (rect == null)
            rect = this.spriteSheet.getFrame(0).rect;
        if (rect == null)
            rect = {width: this.spriteSheet._frameWidth, height: this.spriteSheet._frameHeight};
        return rect;
    }
    
    /**
     * Initialize the graphics of the given monster
     */
    init(monster) {
        
        // create monster sprite
        this.sprite = new createjs.Sprite(this.spriteSheet, "walk_right");
        this.addChild(this.sprite);
        
        // get monster rect
        let rect = this._getSpriteRect();
        
        // calculate x, y, regX, regY
        this.sprite.x = rect.width / 2;
        this.sprite.y = rect.height / 2;
        this.sprite.regX = rect.width / 2;
        this.sprite.regY = rect.height / 2;
        
        // create health bar
        this.healthBar = new TDLoadingBar(game, rect.width, 5);
        this.healthBar.updatePercent(100);
        this.addChild(this.healthBar);
        
        // initial position
        this.regX = rect.width / 2;
        this.regY = rect.height;
        
        this.on("click", this.handleClick);
    }
    
    /**
     * Update the given monster
     */
    update(event, monster) {
        
        // don't do anything if monster is still running on the same direction
        if (monster.lastDirection === monster.direction)
            return;
        
        // update sprite with monster direction
        switch (monster.direction) {
            case "U":
                this.sprite.gotoAndPlay("walk_up");
                this.sprite.scaleX = 1;
                break;
            case "D":
                this.sprite.gotoAndPlay("walk_down");
                this.sprite.scaleX = 1;
                break;
            case "R":
                this.sprite.gotoAndPlay("walk_right");
                this.sprite.scaleX = 1;
                break;
            case "L":
                this.sprite.gotoAndPlay("walk_right");
                this.sprite.scaleX = -1;
                break;
        }
    }
    
    /**
     * Get the center position of the given monster
     */
    getCenterPosition(monster) {
        let rect = this._getSpriteRect(monster);
        let position = {
            x: this.x,
            y: this.y - rect.height / 2
        };
        return position;
    }
    
    /**
     * Update the life bar of the given monster
     */
    updateLife(monster) {
        let percent = monster.health / monster.maxHealth * 100;
        this.healthBar.updatePercent(percent);
    }
    
    /**
     * Update the selection container
     */
    updateSelectionContainer(cnt) {
        if (monster.isDead || monster.isSurvived)
            return;
        
        cnt.addText(monster.name)
            .addSpace(20)
            .addIcon("Lifes")
            .addText(`${monster.health} / ${monster.maxHealth}`)
            .addSpace(20)
            .addIcon("MonsterSpeed")
            .addText(monster.speed);
    }
}