/**
 * Monster factory: let's try to share data between monsters!
 */
class MonsterFactory {
    constructor(monsters) {
        this.monsters = monsters;
        
        this._pathComponent = null;
        this._spriteSheets = {};
        this._spriteSheetsLoaded = 0;
        
        this._loadSpriteSheets();
    }
    
    /**
     * Load all monsters spritesheet
     */
    _loadSpriteSheets() {
        utils.logMethod(this, '_loadSpriteSheets');
        
        for (let key of Object.keys(this.monsters)) {
            let monster = this.monsters[key];
            
            let images = [];
            for (let image of monster.spriteSheet.images)
                images.push(utils.getFile(image));
            
            let spriteSheet = new createjs.SpriteSheet({
                images: images,
                frames: monster.spriteSheet.frames,
                animations: monster.spriteSheet.animations
            });
            //spriteSheet.on('complete', this._handleSpriteSheetComplete, this);
            this._spriteSheets[key] = spriteSheet;
        }
    }
    
    /**
     * Callback when a sprite sheet is loaded
     */
    _handleSpriteSheetComplete(event) {
        this._spriteSheetsLoaded += 1;
        utils.logMethod(this, '_handleSpriteSheetComplete', this._spriteSheetsLoaded);
        
        if (this._spriteSheetsLoaded === this.monsters.length)
            utils.log("OK");
    }
    
    /**
     * Set the current path for the factory
     */
    set currentPath(value) {
        if (value == null)
            throw 'MonsterFactory.currentPath could not be null';
        this._pathComponent = new PathComponent(value)
    }
    
    /**
     * Create and return a new monster
     */
    createMonster(model) {
        // we can not create monster with path component
        if (this._pathComponent === null)
            throw 'MonsterFactory.currentPath could not be null';
        
        let data = this.monsters[model];
        let spriteSheet = this._spriteSheets[model];
        
        let graphics = new MonsterGraphicsComponent(spriteSheet);
        
        let monster = new Monster(data, this._pathComponent, graphics);
        return monster;
    }
}