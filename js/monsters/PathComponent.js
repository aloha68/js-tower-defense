/**
 * PathComponent: component to help a monster to find his way
 */
class PathComponent {
    constructor(path) {
        this.path = path;
    }
    
    /**
     * Get the next position for the given path index
     */
    _getNextPosition(pathIndex) {
        let nextPosition = Object.assign({}, this.path[pathIndex]);
        nextPosition.x = nextPosition.x * GAME_TILE_SIZE + GAME_TILE_SIZE / 2;
        nextPosition.y = nextPosition.y * GAME_TILE_SIZE + GAME_TILE_SIZE / 2;
        return nextPosition;
    }
    
    /**
     * Get the next direction for a given monster
     */
    _calculateNextDirection(monster, nextPosition) {
        monster.lastDirection = monster.direction;
        if (monster.x < nextPosition.x) {
            monster.direction = "R";
        } else if (monster.x > nextPosition.x) {
            monster.direction = "L";
        } else if (monster.y < nextPosition.y) {
            monster.direction = "D";
        } else if (monster.y > nextPosition.y) {
            monster.direction = "U";
        } else {
            throw "Monster: Unknown direction";
        }
    }
    
    /**
     * Move the given monster
     */
    _move(monster) {
        switch (monster.direction) {
            case "U":
                monster.y -= monster.speed;
                break;
            case "D":
                monster.y += monster.speed;
                break;
            case "R":
                monster.x += monster.speed;
                break;
            case "L":
                monster.x -= monster.speed;
                break;
        }
    }
    
    /**
     * Check next position after move
     */
    _checkNextPosition(monster, nextPosition) {
        let pathPast = false;
        
        if (monster.direction === "U" && monster.y <= nextPosition.y)
            pathPast = true;
        else if (monster.direction === "D" && monster.y >= nextPosition.y)
            pathPast = true;
        else if (monster.direction === "R" && monster.x >= nextPosition.x)
            pathPast = true;
        else if (monster.direction === "L" && monster.x <= nextPosition.x)
            pathPast = true;
        
        if (pathPast)
            monster.currentPath += 1;
    }
    
    /**
     * Initialize the given monster to follow path
     */
    init(monster) {
        let position = this._getNextPosition(0);
        monster.x = position.x;
        monster.y = position.y;
        monster.currentPath = 1;
    }
    
    /**
     * Update the given monster
     */
    update(event, monster) {
        
        // we can only move monsters
        if (!(monster instanceof Monster))
            throw "Can only get next direction for a monster";
        
        let nextPosition = this._getNextPosition(monster.currentPath);
        this._calculateNextDirection(monster, nextPosition);
        this._move(monster);
        this._checkNextPosition(monster, nextPosition);
        
        // if all path is done, monster is survived
        if (monster.currentPath === this.path.length)
            monster.survive();
    }
}