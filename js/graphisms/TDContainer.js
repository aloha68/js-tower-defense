/**
 * Custom class to handle EaselJS container
 */
class TDContainer extends createjs.Container {
    constructor(game) {
        super();
        this.game = game;
        this.data = this.game.data;
    }
    
    /**
     * Get a preloaded file from its name or its path
     * @param fileName
     */
    getFile(fileName) {
        return this.game.getFile(fileName);
    }
    
    /**
     * Set the regX and regY from an object
     * Anchor:
     *  "NW"   "N"          "NE"
     *  "W"    "Default"    "E"
     *  "SW"   "S"          "SE"
     */
    setObjectAnchor(obj, anchor="", width=0, height=0) {
        
        // Look for object width if needed
        if (width <= 0) {
            if (obj instanceof createjs.Bitmap)
                width = obj.image.width;
            else if (obj instanceof createjs.Text)
                width = obj.getMeasuredWidth();
        }
        
        // Look for object height if needed
        if (height <= 0) {
            if (obj instanceof createjs.Bitmap)
                height = obj.image.height;
            else if (obj instanceof createjs.Text)
                height = obj.getMeasuredHeight();
        }
        
        // Set vertical anchor
        if (anchor.indexOf('N') >= 0)
            obj.regY = 0;
        else if (anchor.indexOf('S') >= 0)
            obj.regY = height;
        else
            obj.regY = height / 2;
        
        // Set horizontal anchor
        if (anchor.indexOf('W') >= 0)
            obj.regX = 0;
        else if (anchor.indexOf('E') >= 0)
            obj.regX = width;
        else
            obj.regX = width / 2;
        
        return obj;
    }
    
    /**
     * Create an EaselJS text, put it registration point on bottom-center and return it
     */
    createText(text="", anchor="", font="", color="") {
        let obj = new createjs.Text(text, font, color);
        this.setObjectAnchor(obj, anchor);
        return obj;
    }
    
    /**
     * Create a icon from image name
     */
    createIcon(imageName, size=0, anchor="") {
        let image = this.getFile(imageName);
        let icon = new createjs.Bitmap(image);
        
        if (size > 0) {
            icon.scaleX = size / image.width;
            icon.scaleY = size / image.height;
        }
        
        this.setObjectAnchor(icon, anchor, size, size);
        return icon;
    }
    
    /**
     * Log a message
     */
    log(message, methodName=null, args=null) {
        let msg = this.constructor.name;
        if (methodName !== null) {
            msg += "." + methodName + "(";
            if (args !== null)
                msg += args;
            msg += ")";
        }
        msg += ": " + message;
        console.log(msg);
    }
    
    /**
     * Log a message with method name
     */
    logMethod(methodName, args=null) {
        let msg = this.constructor.name + "." + methodName + "(";
        if (args != null) {
            if (typeof args === 'string')
                msg += `"${args}"`;
            else
                msg += args;
        }
        msg += ")";
        console.log(msg);
    }
    
    /**
     * Log a message with property name
     */
    logProperty(propertyName, value) {
        let msg = `${this.constructor.name}.${propertyName}`;
        
        if (value === null || !Array.isArray(value) && typeof value !== 'object') {
            msg += ` = ${value}`;
            console.log(msg);
        } else {
            msg += " has a new value!";
            console.log(msg);
            console.log(value);
        }
    }
    
    /**
     * Remove all children and event listeners from object and children
     */
    removeAll() {
        if (this.children) {
            for (let child of this.children) {
                if (child instanceof TDContainer)
                    child.removeAll();
                if (child instanceof createjs.Container)
                    child.removeAllChildren();
                if (child instanceof createjs.DisplayObject)
                    child.removeAllEventListeners();
            }
            this.removeAllChildren();
        }
        this.removeAllEventListeners();
    }
}