/**
 * Floating menu button
 */
class TDFloatingMenuButton extends TDContainer {
    constructor(game, imageName) {
        super(game);
        this.imageName = imageName;
        this.image = null;
        this._isActive = true;
        this.activeCondition = null;
        this.rect = new createjs.Rectangle();
        this.init();
    }
    
    /**
     * Init the floating button
     */
    init() {
        this.name = "menuButton" + this.imageName;
        this.cursor = "pointer";
        this.on("tick", this.handleTick);
        
        this.image = this.getFile(this.imageName);
        this.rect.x = (0 - this.image.width / 2) - 5;
        this.rect.y = (0 - this.image.height / 2) - 5;
        this.rect.width = this.image.width + 10;
        this.rect.height = this.image.height + 10;
        
        let strokeThickness = 1;
        let rect = new createjs.Shape();
        rect.graphics
            .setStrokeStyle(strokeThickness)
            .beginStroke("#777")
            .beginFill("#eee")
            .drawRoundRect(this.rect.x, this.rect.y, this.rect.width, this.rect.height, 3);
        rect.alpha = 0.7;
        rect.name = "rectIcon" + this.imageName;
        this.addChild(rect);
        
        this.rect.x -= strokeThickness;
        this.rect.y -= strokeThickness;
        this.rect.width += strokeThickness * 2;
        this.rect.height += strokeThickness * 2;
        
        let icon = new createjs.Bitmap(this.image);
        icon.name = "icon" + this.imageName;
        icon.regX = this.image.width / 2;
        icon.regY = this.image.height / 2;
        this.addChild(icon);
        
        this.cache(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
    }
    
    /**
     * Return the active state of the button
     */
    get active() {
        return this._isActive;
    }
    
    /**
     * Set the active state of the button
     */
    set active(value) {
        if (this._isActive == value)
            return;
        
        this._isActive = value;
        if (this._isActive) {
            this.filters = [];
            this.cursor = "pointer";
        } else {
            let matrix = new createjs.ColorMatrix();
            matrix.adjustSaturation(-100);
            this.filters = [new createjs.ColorMatrixFilter(matrix)];
            this.cursor = null;
        }
        this.updateCache();
    }
    
    /**
     * Add a bottom text to the button
     */
    addBottomText(text) {
        let container = new createjs.Container();
        let t = this.createText(text, "S");
        
        let rectWidth = t.getMeasuredWidth() + 5;
        let rectHeight = t.getMeasuredHeight() + 5;
        
        let rect = new createjs.Shape();
        rect.graphics
            .beginStroke("#777")
            .beginFill("#eee")
            .drawRoundRect(0, 0, rectWidth, rectHeight, 3);
        rect.regX = rectWidth / 2;
        rect.regY = rectHeight;
        rect.alpha = 0.75;
        
        container.addChild(rect);
        container.addChild(t);
        
        container.y = this.rect.height - 8;
        this.addChild(container);
        
        if (rectWidth > this.rect.width)
            this.rect.width = rectWidth;
        this.rect.height += rectHeight;
        
        this.cache(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
    }
    
    /**
     * Callback for tick (to update active state)
     */
    handleTick(event) {
        if (typeof(this.activeCondition) === 'function') {
            this.active = this.activeCondition();
        }
    }
}

/**
 * Floating menu
 */
class TDFloatingMenu extends TDContainer {
    constructor(game) {
        super(game);
        this.init();
    }
    
    /**
     * Init the floating menu
     */
    init() {
        this.removeAllChildren();
        this.removeAllEventListeners();
        
        this.regX = MENU_SIZE / 2;
        this.regY = MENU_SIZE / 2;
        
        // Create circle around menu
        let menuBg = new createjs.Shape();
        menuBg.name = "floatingMenuBackground";
        menuBg.graphics
            .setStrokeStyle(3, "round")
            .beginStroke("#777")
            .drawCircle(MENU_SIZE / 2, MENU_SIZE / 2, MENU_SIZE / 2);
        menuBg.alpha = 0.5;
        this.addChild(menuBg);
    }
    
    /**
     * Add a button to the floating menu
     */
    addButton(button, position) {
        switch (position) {
            case 'N':
                button.x = MENU_SIZE / 2;
                button.y = 0;
                break;
            case 'NE':
                button.x = MENU_SIZE * 0.85;
                button.y = MENU_SIZE * 0.15;
                break;
            case 'E':
                button.x = MENU_SIZE;
                button.y = MENU_SIZE / 2;
                break;
            case 'SE':
                button.x = MENU_SIZE * 0.85;
                button.y = MENU_SIZE * 0.85;
                break;
            case 'S':
                button.x = MENU_SIZE / 2;
                button.y = MENU_SIZE;
                break;
            case 'SW':
                button.x = MENU_SIZE * 0.15;
                button.y = MENU_SIZE * 0.85;
                break;
            case 'W':
                button.x = 0;
                button.y = MENU_SIZE / 2;
                break;
            case 'NW':
                button.x = MENU_SIZE * 0.15;
                button.y = MENU_SIZE * 0.15;
                break;
        }
        this.addChild(button);
    }
}