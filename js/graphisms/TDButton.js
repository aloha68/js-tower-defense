/**
 * TDButton: A generic button. Icon should be 32x32px
 */
class TDButton extends TDContainer {
    constructor(game, imageName, text="", width=50, height=50, background="#eee", border="#777") {
        super(game);
        
        this.text = text;
        this.imageName = imageName;
        this.width = width;
        this.height = height;
        this.background = background;
        this.border = border;
        
        this.init();
    }
    
    init() {
        this.cursor = "pointer";
        
        this.image = this.getFile(this.imageName);

        // background
        let background = new createjs.Shape();
        background.graphics
            .setStrokeStyle(1)
            .beginStroke(this.border)
            .beginFill(this.background)
            .drawRoundRect(0, 0, this.width, this.height, 3);
        background.alpha = 0.7;
        this.addChild(background);
        
        // icon
        let icon = new createjs.Bitmap(this.image);
        icon.regX = this.image.width / 2;
        icon.regY = this.image.height / 2;
        icon.x = this.width / 2;
        icon.y = this.height / 2;
        this.addChild(icon);
        
        // text
        if (this.text) {
            let text = this.createText(this.text, "S", "18px JWerd", "black");
            text.x = this.width / 2;
            text.y = this.height - 5;
            this.addChild(text);
            
            icon.y -= 5;
        }
    }
}