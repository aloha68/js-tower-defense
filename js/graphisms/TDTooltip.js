/**
 * Tooltip to show
 */
class TDTooltip extends createjs.DOMElement {
    constructor(eventData) {
        super(TOOLTIP_ELEMENT);
        
        this.eventData = eventData;
        
        this.titleElement = document.getElementById(TOOLTIP_TITLE_ELEMENT);
        if (this.titleElement === null)
            throw new Error("Tooltip title element could not be found: " + TOOLTIP_TITLE_ELEMENT);
        
        this.bodyElement = document.getElementById(TOOLTIP_BODY_ELEMENT);
        if (this.bodyElement === null)
            throw new Error("Tooltip body element could not be found: " + TOOLTIP_BODY_ELEMENT);
        
        this.htmlElement.style['color'] = TOOLTIP_FONT_COLOR;
        this.htmlElement.style['border'] = `1px ${TOOLTIP_BORDER_COLOR} solid`;
        this.htmlElement.style['background-color'] = TOOLTIP_BACKGROUND_COLOR;
        this.bodyElement.innerHTML = "";
        this.bodyElement.style.display = "none";
    }
    
    /**
     * Return the tooltip width
     */
    get width() {
        return this.htmlElement.clientWidth;
    }
    
    /**
     * Return the tooltip height
     */
    get height() {
        return this.htmlElement.clientHeight;
    }
    
    /**
     * Set the tooltip title
     */
    set title(value) {
        this.titleElement.innerHTML = value;
    }
    
    /**
     * Add a HTML line to the tooltip
     */
    addLine(line) {
        this.bodyElement.innerHTML += `<p>${line}</p>`;
        this.bodyElement.style.display = "block";
    }
}