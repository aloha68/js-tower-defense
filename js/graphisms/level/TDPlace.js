/**
 * Place: a common place to build tower
 */
class TDPlace extends TDLevelSelectableContainer {
    constructor(parent, tower=null) {
        super(parent.level);
        this.parent = parent;
        this.tower = tower;
        this.rangeTooltip = null;
        this.tsLastAttack = null;
        this.init();
    }
    
    /**
     * Init the place
     */
    init() {
        this.cursor = "pointer";
        
        if (this.tower == null) {
            this.drawBuildingPlace();
        } else {
            this.drawTower();
        }
    }
    
    /**
     * Clear the current place
     */
    clear() {
        this.removeAllChildren();
        if (this.rangeTooltip != null)
            this.parent.removeChild(this.rangeTooltip);
        
        this.off("rollover", this.handleMouseOver);
        this.off("rollout", this.handleMouseOut);
    }
    
    /**
     * Draw a clean building place
     */
    drawBuildingPlace() {
        this.clear();
        
        let circle = new createjs.Shape();
        circle.graphics.beginFill("#eeeeee").drawCircle(0, 0, 24);
        this.addChild(circle);  
        
        let icon = new createjs.Bitmap(this.getFile("buildingPlace"));
        icon.x = 0;
        icon.y = 0;
        icon.regX = 16;
        icon.regY = 16;
        this.addChild(icon);
    }
    
    /**
     * Draw a tower
     */
    drawTower() {
        this.clear();
        
        let circle = new createjs.Shape();
        circle.graphics.beginFill("#eeeeee").drawCircle(0, 0, 24);
        this.addChild(circle);
        
        let image = this.getFile(this.tower.icon);
        let icon = new createjs.Bitmap(image);
        icon.x = 0;
        icon.y = 0;
        icon.regX = 16;
        icon.regY = 16;
        this.addChild(icon);
        
        this.rangeTooltip = this.createRangeTooltip();
        this.parent.addChildAt(this.rangeTooltip, 0);
        
        this.on("rollover", this.handleMouseOver);
        this.on("rollout", this.handleMouseOut);
    }
    
    /**
     * Create the range tooltip
     */
    createRangeTooltip(range=null, fillColor="#eee", borderColor="black") {
        if (range == null && this.tower == null)
            throw "TDPlace: can't create range tooltip without tower or given range";
        
        if (range == null)
            range = this.tower.range;
        
        let rangeTooltip = new createjs.Shape();
        rangeTooltip.graphics
            .setStrokeStyle(2)
            .setStrokeDash([5, 10])
            .beginStroke(borderColor)
            .beginFill(fillColor)
            .drawCircle(this.x, this.y, range);
        
        rangeTooltip.alpha = 0.2;
        rangeTooltip.visible = false;
        
        return rangeTooltip;
    }
    
    /**
     * Set current tower
     */
    setTower(tower) {
        this.tower = tower;
        this.drawTower();
    }
    
    /**
     * Remove current tower
     */
    removeTower() {
        this.tower = null;
        this.drawBuildingPlace();
    }
    
    /**
     * Return true if tower can attack
     */
    canAttack(monster) {
        if (this.tower === null)
            return false;
        
        if (this.tsLastAttack != null) {
            let diffBetweenTs = (Date.now() - this.tsLastAttack) / 1000;
            if (diffBetweenTs < this.tower.reloadTime)
                return false;
        }
        let position = monster.getPosition();
        return this.rangeTooltip.hitTest(position.x, position.y)
    }
    
    /**
     * Attack a monster and create a new projectile
     */
    attack(monster) {
        if (this.tower === null)
            return;
        
        this.tsLastAttack = Date.now();
        
        // create projectile
        let proj = utils.projectileFactory.createProjectile(monster, this.x, this.y);
        proj.on('hit', this.handleProjectileHit, this);
        proj.on('miss', this.handleProjectileMiss, this);
        this.parent.addChild(proj.graphics);
    }
    
    /**
     * Callback when a projectile hit target
     */
    handleProjectileHit(event) {
        let proj = event.currentTarget;
        
        let min = Math.ceil(this.tower.damageMin),
            max = Math.floor(this.tower.damageMax),
            damage = Math.floor(Math.random() * (max - min + 1)) + min;
        
        //console.log(`Tower ${this.tower.id} damage ${damage} to ${proj.target}`)
        
        proj.target.loseHealth(damage);
        this.parent.removeChild(proj.graphics);
    }
    
    /**
     * Callback when a projectile hit target
     */
    handleProjectileMiss(event) {
        let proj = event.currentTarget;
        this.parent.removeChild(proj.graphics);
    }
    
    /**
     * Callback when mouse over (to show the range of the tower)
     */
    handleMouseOver(event) {
        this.rangeTooltip.visible = true;
    }
    
    /**
     * Callback when mouse out (to hide the range of the tower)
     */
    handleMouseOut(event) {
        this.rangeTooltip.visible = false;
    }
    
    /**
     * Update the selection container
     */
    updateSelectionContainer(cnt) {
        if (this.tower == null)
            return;
        
        cnt.addText(this.tower.name)
            .addSpace(20)
            .addIcon("TowerDamage")
            .addText(`${this.tower.damageMin}-${this.tower.damageMax}`)
            .addSpace(20)
            .addIcon("TowerRange")
            .addText(`${this.tower.range}`)
            .addSpace(20)
            .addIcon("TowerReloadTime")
            .addText(`${this.tower.reloadTime}`);
    }
}