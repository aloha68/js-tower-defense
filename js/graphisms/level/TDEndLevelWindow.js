/**
 * TDEndLevelWindow: ending window if player finish a level
 */
class TDEndLevelWindow extends TDWindow {
    constructor(level, isSuccess) {
        let title = isSuccess ? "Level completed" : "Level failed";
        super(level, 500, 400, title);
        this.isSuccess = isSuccess;
        this.init();
    }
    
    /**
     * Init the end success window
     */
    init() {
        super.init();
        
        let message = "";
        if (this.isSuccess)
            message = "Congratulations, this level was easy after all!";
        else
            message = "No luck, you will succeed next time!";
        
        let text = this.createText(message, "N", "24px JWerd", "black");
        text.x = this.width / 2;
        text.y = 100;
        this.addChild(text);
        
        let btnRestart = new TDButton(this.game, "buttonRestartLevel", "Restart level", 120, 70);
        btnRestart.x = 100;
        btnRestart.y = 300;
        btnRestart.on("click", this.handleButtonClick, this, false, {e: "restart"});
        this.addChild(btnRestart);
        
        let btnMenu = new TDButton(this.game, "buttonReturnMenu", "Return to menu", 120, 70);
        btnMenu.x = 280;
        btnMenu.y = 300;
        btnMenu.on("click", this.handleButtonClick, this, false, {e: "menu"});
        this.addChild(btnMenu);
    }
    
    /**
     * Callback when a button is clicked
     */
    handleButtonClick(event, data) {
        let e = new createjs.Event(data.e);
        this.dispatchEvent(e);
    }
}