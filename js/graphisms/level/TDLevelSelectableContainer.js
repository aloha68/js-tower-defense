/**
 * LevelSelectableContainer: a level container which can be selected
 */
class TDLevelSelectableContainer extends TDLevelContainer {
    constructor(level) {
        super(level);
        this.isSelected = false;
    }
    
    /**
     * Send data changed event to all listeners
     */
    sendDataChangedEvent() {
        let event = new createjs.Event("datachange");
        this.dispatchEvent(event);
    }
    
    /**
     * Set the current object as level selection
     */
    select() {
        if (!this.isSelected)
            this.level.selectObject(this);
    }
    
    /**
     * Unset the current object as level selection
     */
    unselect() {
        if (this.isSelected)
            this.level.clearSelection();
    }
    
    /**
     * Update the selection container
     * Every TDLevelSelectableContainer has to implement this function
     */
    updateSelectionContainer(cnt) {
        throw new Error("You have to implement updateSelectionContainer(cnt)");
    }
    
    /**
     * Callback on click
     */
    handleClick(event) {
        event.stopPropagation();
        this.select();
    }
}