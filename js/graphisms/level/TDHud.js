/**
 * TDHud: level HUD
 */
class TDHud extends TDLevelContainer {
    constructor(level) {
        super(level);
        
        this.textColor = "#EEE";
        this.backgroundColor = "#222";
        this.backgroundAlpha = 0.7;
        this.borderTickness = 1;
        this.borderColor = "#EEE";
        
        this.texts = {};
        this.statusText = null;
        this.btnStartWave = null;
        
        this._selectedTarget = null;
        this.selection = null;
        this.tooltip = null;
        
        this.init();
    }
    
    /**
     * Init the level HUD
     */
    init() {
        this.logMethod("init");
        
        // Level data
        this.createRect(10, 10, 135, 50);
        this.createData("Lifes", "99", 15, 15);
        this.createData("Gold", "999999", 65, 15);
        this.createData("WaveNumber", "Wave: 99/99", 25, 35);
        
        // Pause button
        let btn = this.createButton("Pause", this.game.canvas.width - 39, 15, 24);
        btn.on("click", this.handleButtonClick, this, false, {e: "pause"});
        btn.on("rollover", this.handleButtonMouseOver, this, false, {e: "pause"});
        btn.on("rollout", this.handleButtonMouseOut, this, false, {e: "pause"});
        
        // Status bar
        let canvas = this.game.canvas;
        this.createRect(0, canvas.height-40, canvas.width, 40, "#EEE", 1);
        
        // Selection
        this.selection = new TDSelection(this.level, 0, canvas.height-40, canvas.width, 40);
        this.addChild(this.selection);
        
        this.levelData.on("change", this.handleDataChange, this);
        this.levelData.sendChangedEvent();
    }
    
    /**
     * Create a rectangle directly on the level
     */
    createRect(x, y, width, height, color="", alpha=0) {
        
        if (color.length == 0)
            color = this.backgroundColor;
        if (alpha <= 0)
            alpha = this.backgroundAlpha;
        
        let rect = new createjs.Shape();
        rect.graphics
            .beginFill(color)
            .drawRect(x, y, width, height);
        rect.alpha = alpha;
        this.addChild(rect);
    }
    
    /**
     * Create a level data
     * We consider there is a image which is called "hud" + data name
     */
    createData(name, data, x, y) {
        let container = new createjs.Container();
        
        let icon = this.createIcon("hud" + name, 16, "W");
        icon.x = 0;
        icon.y = 7;
        container.addChild(icon);
        
        let text = this.createText(data, "SW", "18px JWerd", this.textColor);
        text.x = 25;
        text.y = 16;
        container.addChild(text);
        this.texts[name] = text;
        
        container.x = x;
        container.y = y;
        this.addChild(container);
        
        return container;
    }
    
    /**
     * Create a button
     */
    createButton(imageName, x, y, size=0, anchor="") {
        
        let button = new TDLevelContainer(this.level);
        button.cursor = "pointer";
        
        let bg = new createjs.Shape();
        bg.graphics
            .setStrokeStyle(this.borderTickness)
            .beginStroke(this.borderColor)
            .beginFill(this.backgroundColor)
            .drawRoundRect(-5, -5, size+10, size+10, 2);
        bg.alpha = this.backgroundAlpha;
        button.addChild(bg);
        
        let icon = this.createIcon("hud" + imageName, size, "NW");
        button.addChild(icon);
        
        this.setObjectAnchor(button, anchor);
        button.x = x;
        button.y = y;
        button.width = size + 10;
        button.height = size + 10;
        this.addChild(button);
        
        return button;
    }
    
    /**
     * Show the start wave button
     */
    showStartWaveButton(x, y) {
        
        // Button creation
        if (this.btnStartWave === null) {
            
            if (x <= 0)
                x = 10;
            if (y <= 0)
                y = 10;
            
            let data = {e: "startWave"};
            
            this.btnStartWave = this.createButton("StartWave", x, y, 16);
            this.btnStartWave.on("click", this.handleButtonClick, this, false, data);
            this.btnStartWave.on("rollover", this.handleButtonMouseOver, this, false, data);
            this.btnStartWave.on("rollout", this.handleButtonMouseOut, this, false, data);
        }
        
        this.btnStartWave.visible = true;
    }
    
    /**
     * Show a tooltip
     */
    showTooltip(tooltip, x, y) {
        //this.logMethod("showTooltip");
        
        this.tooltip = tooltip;
        
        let canvas = this.game.canvas;
        
        if (x < 10)
            x = 10;
        if (y < 10)
            y = 10;
        
        if (x + this.tooltip.width > canvas.width - 10)
            x = canvas.width - this.tooltip.width - 10;
        if (y + this.tooltip.height > canvas.height - 10)
            y = canvas.height - this.tooltip.height - 10;
        
        this.tooltip.x = x;
        this.tooltip.y = y;
        
        // x correction
        this.tooltip.x += canvas.offsetLeft;
        
        this.addChild(this.tooltip);
    }
    
    /**
     * Hide the current tooltip
     */
    hideTooltip() {
        //this.logMethod("hideTooltip");
        
        this.tooltip.visible = false;
        this.removeChild(this.tooltip);
        this.tooltip = null;
    }
    
    /**
     * Return the current selected target
     */
    get selectedTarget() {
        return this._selectedTarget;
    }
    
    /**
     * Set the current selected target
     */
    set selectedTarget(value) {
        if (this._selectedTarget == value)
            return;
        
        //this.logProperty("selectedTarget", value);
        if (this._selectedTarget != null) {
            this._selectedTarget.isSelected = false;
            this._selectedTarget.off("datachanged");
        }
        
        this._selectedTarget = value;
        this.selection.clear();
        
        if (this._selectedTarget != null) {
            this._selectedTarget.isSelected = true;
            this._selectedTarget.on("datachange", this.handleSelectionDataChange, this);
            this._selectedTarget.updateSelectionContainer(this.selection);
        }
    }
    
    /**
     * Change the current selection for a custom message
     */
    updateStatus(message) {
        this.selectedTarget = null;
        this.selection.clear();
        this.selection.addText(message);
    }
    
    /**
     * Callback when level data changed
     */
    handleDataChange(event) {
        let data = this.levelData;
        this.texts["Gold"].text = data.gold;
        this.texts["Lifes"].text = data.lifes;
        this.texts["WaveNumber"].text = `Wave: ${data.numWave}/${data.numWaves}`;
    }
    
    /**
     * Callback when selection data changed
     */
    handleSelectionDataChange(event) {
        let target = event.currentTarget;
        if (target == this._selectedTarget) {
            this.selection.clear();
            this._selectedTarget.updateSelectionContainer(this.selection);
        }
    }
    
    /**
     * Callback when a buttun is clicked
     */
    handleButtonClick(event, data) {
        event.stopPropagation();
        
        if (data.e == "startWave")
            this.btnStartWave.visible = false;
        
        this.dispatchEvent(new createjs.Event(data.e));
    }
    
    /**
     * Callback when a button is targetted by the mouse
     */
    handleButtonMouseOver(event, data) {
        event.stopPropagation();
        
        if (this.tooltip !== null && this.tooltip.eventData == data.e)
            return;
        
        let tooltip = new TDTooltip(data.e);
        
        switch(data.e) {
            
            case "pause":
                tooltip.title = "Pause the game";
                break;
                
            case "startWave":
                let waveNumber = this.levelData.numWave + 1;
                tooltip.title = `Start wave ${waveNumber}`;
                
                let waveMonsters = {};
                for (let m of this.levelData.waves[waveNumber-1].monsters) {
                    let model = this.data.monsters[m.name];
                    if (!waveMonsters.hasOwnProperty(model.name))
                        waveMonsters[model.name] = 0;
                    waveMonsters[model.name] += 1;
                }
                
                for (let key of Object.keys(waveMonsters))
                    tooltip.addLine(`${waveMonsters[key]} ${key}`);
                
                break;
        }
        
        this.showTooltip(tooltip, event.stageX, event.stageY);
    }
    
    /**
     * Callback when a button is leaved by the mouse
     */
    handleButtonMouseOut(event, data) {
        event.stopPropagation();

        /* Here is some code to fix tooltip hiding but it's not okay
        let button = event.currentTarget,
            mouseX = event.stageX,
            mouseY = event.stageY;

        if (button.x <= mouseX && (button.x + button.width) >= mouseX
           && button.y <= mouseY && (button.y + button.height) >= mouseY) {
            console.log("mouse in button");
            return;
        }
        */
        
        if (this.tooltip !== null)
            this.hideTooltip();
    }
}