/**
 * LevelContainer: a intermediate container to add level and levelData property
 */
class TDLevelContainer extends TDContainer {
    constructor(level) {
        super(level.game);
        this.level = level;
        this.levelData = level.levelData;
    }
}