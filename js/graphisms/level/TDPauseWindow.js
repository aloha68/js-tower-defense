/**
 * TDPauseWindow: pause window
 */
class TDPauseWindow extends TDWindow {
    constructor(level) {
        super(level, 500, 400, "Pause");
        this.init();
    }
    
    /**
     * Init the pause window
     */
    init() {
        super.init();
        
        let btnResume = new TDButton(this.game, "buttonResume", "Resume", 120, 70);
        btnResume.x = 190;
        btnResume.y = 200;
        btnResume.on("click", this.handleButtonClick, this, false, {e: "resume"});
        this.addChild(btnResume);
        
        let btnRestart = new TDButton(this.game, "buttonRestartLevel", "Restart level", 120, 70);
        btnRestart.x = 100;
        btnRestart.y = 300;
        btnRestart.on("click", this.handleButtonClick, this, false, {e: "restart"});
        this.addChild(btnRestart);
        
        let btnMenu = new TDButton(this.game, "buttonReturnMenu", "Return to menu", 120, 70);
        btnMenu.x = 280;
        btnMenu.y = 300;
        btnMenu.on("click", this.handleButtonClick, this, false, {e: "menu"});
        this.addChild(btnMenu);
    }
    
    /**
     * Callback when a button is clicked
     */
    handleButtonClick(event, data) {
        let e = new createjs.Event(data.e);
        this.dispatchEvent(e);
    }
}