/**
 * Background: contains all tiles to create background
 */
class TDBackground extends TDLevelContainer {
    constructor(level, tilesData) {
        super(level);
        
        this.tilesData = tilesData;
        this.spriteSheet = null;
        this.mapData = [];
        this.layers = [];
        this.isReady = false;
        
        this.init();
    }
    
    /**
     * Init the background 
     */
    init() {
        this.spriteSheet = new createjs.SpriteSheet({
            images: [this.tilesData.image],
            frames: {
                width: this.tilesData.tilewidth,
                height: this.tilesData.tileheight,
                count: this.tilesData.tilecount,
                spacing: this.tilesData.spacing,
                margin: this.tilesData.margin,
                regX: 0,
                regY: 0
            }
        });
    }
    
    /**
     * Add a layer to the current background
     */
    addLayer(layer) {
        this.layers.push(layer);
    }
    
    /**
     * Draw a layer to the current background
     */
    drawLayer(layer) {
        this.logMethod("drawLayer", layer.name);
        
        let container = new createjs.Container();
        container.name = "cnt" + layer.name;
        
        let tileIndex = 0;
        for (let y = 0; y < layer.height; y++) {
            
            for (let x = 0; x < layer.width; x++) {
                let tileNumber = layer.data[tileIndex] - 1;
                
                if (layer.name == "Path") {
                    if (typeof(this.mapData[y]) == 'undefined') {
                        this.mapData[y] = [];
                    }
                    this.mapData[y].push(tileNumber >= 0 ? 1 : 0);
                }
                
                if (tileNumber >= 0) {
                    let tile = new createjs.Sprite(this.spriteSheet);
                    tile.gotoAndStop(tileNumber);
                    tile.x = x * GAME_TILE_SIZE;
                    tile.y = y * GAME_TILE_SIZE;
                    container.addChild(tile);
                }
                
                tileIndex += 1;
            }
        }
        
        this.addChild(container);
    }
    
    /**
     * Draw all layers of the background
     */
    drawAllLayers() {
        
        // If spritesheet is not loading, add an event on it
        if (!this.spriteSheet.complete) {
            this.spriteSheet.on("complete", this.drawAllLayers, this);
            return;
        }
        
        for(let layer of this.layers) {
            this.drawLayer(layer);
        }
        
        this.isReady = true;
        
        let event = new createjs.Event("ready");
        event.mapData = this.mapData;
        this.dispatchEvent(event);
    }
}