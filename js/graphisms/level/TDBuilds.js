/**
 * Builds: contains all building places on a level
 */
class TDBuilds extends TDLevelContainer {
    constructor(level) {
        super(level);
        
        this.builds = [];
        this.selectedPlace = null;
        this.rangeTooltip = null;
        
        this.buildMenu = null;
        this.upgradeMenu = null;
        this.activeMenu = null;
        
        this.init();
    }
    
    /**
     * Init builds container
     */
    init() {
        this.name = "cntBuilds";
        
        this.buildMenu = this.createBuildMenu();
        this.addChild(this.buildMenu);
        
        this.upgradeMenu = new TDFloatingMenu(this.game);
        this.upgradeMenu.name = "menuUpgradeTower";
        this.upgradeMenu.visible = false;
        this.addChild(this.upgradeMenu);
    }
    
    /**
     * Add a building place to the given loc
     */
    addBuildingPlace(x, y) {
        let buildPlace = new TDPlace(this);
        buildPlace.name = "place" + this.builds.length;
        buildPlace.x = x;
        buildPlace.y = y;
        buildPlace.on("click", this.handleClick, this);
        
        this.builds.push(buildPlace);
        this.addChild(buildPlace);
    }
    
    /**
     * Create a menu button to buy a tower
     */
    createMenuButton(tower) {
        let btn = new TDFloatingMenuButton(this.game, tower.icon);
        btn.addBottomText(tower.price);
        
        let eventData = {tower: tower};
        btn.on("click", this.handleBuildClick, this, false, eventData);
        btn.on("rollover", this.handleTowerMouseOver, this, false, eventData);
        btn.on("rollout", this.handleTowerMouseOut, this, false, eventData);
        btn.activeCondition = () => tower.price <= this.levelData.gold;
        
        return btn;
    }
    
    /**
     * Create the basic build menu
     */
    createBuildMenu() {
        let menu = new TDFloatingMenu(this.game);
        menu.name = "menuBuildTower";
        menu.visible = false;
        
        let btn = this.createMenuButton(this.data.towers[0]);
        menu.addButton(btn, 'N');
        
        btn = this.createMenuButton(this.data.towers[1]);
        menu.addButton(btn, 'E');
        
        btn = this.createMenuButton(this.data.towers[2]);
        menu.addButton(btn, 'S');
        
        btn = this.createMenuButton(this.data.towers[3]);
        menu.addButton(btn, 'W');
        
        return menu;
    }
    
    /**
     * Create the upgrade menu from a tower
     */
    createUpgradeMenu(tower) {
        this.upgradeMenu.init();
        
        // Get buttons for upgradable towers
        let buttons = [];
        for (let t of tower.upgradableTowers) {
            let btn = this.createMenuButton(t);
            buttons.push(btn);
        }
        
        switch(buttons.length) {
            case 1:
                this.upgradeMenu.addButton(buttons[0], 'N');
                break;
            case 2:
                this.upgradeMenu.addButton(buttons[0], 'NW');
                this.upgradeMenu.addButton(buttons[1], 'NE');
                break;
            case 3:
                this.upgradeMenu.addButton(buttons[0], 'N');
                this.upgradeMenu.addButton(buttons[1], 'W');
                this.upgradeMenu.addButton(buttons[2], 'E');
                break;
        }
        
        let sellBtn = new TDFloatingMenuButton(this.game, "sellMenu");
        sellBtn.on("click", this.handleSellClick, this);
        this.upgradeMenu.addButton(sellBtn, 'S');
        
        return this.upgradeMenu;
    }
    
    /**
     * Show the active menu on a given place
     */
    showActiveMenu(place) {
        if (this.activeMenu == null)
            throw "TDBuilds: can't show a null menu";
        
        this.activeMenu.visible = true;
        this.setChildIndex(this.activeMenu, this.numChildren - 1);
        
        let halfWidth = this.activeMenu.getBounds().width / 2;
        let halfHeight = this.activeMenu.getBounds().height / 2;
        
        // set menu X
        this.activeMenu.x = place.x;
        if (place.x - halfWidth < 0) {
            this.activeMenu.x = halfWidth;
        } else if (place.x + halfWidth > this.game.canvas.width) {
            this.activeMenu.x = this.game.canvas.width - halfWidth;
        }
        
        // set menu Y
        this.activeMenu.y = place.y;        
        if (place.y - halfHeight < 0) {
            this.activeMenu.y = halfHeight;
        } else if (place.y + halfHeight > this.game.canvas.height) {
            this.activeMenu.y = this.game.canvas.height - halfHeight;
        }
    }
    
    /**
     * Hide the active menu
     */
    hideActiveMenu() {
        if (this.activeMenu == null)
            return;
        
        this.activeMenu.visible = false;
        this.activeMenu = null;
        this.level.clearSelection();
    }
    
    /**
     * Check for every tower if attack is possible
     */
    checkAttack(monsters) {
        for (let place of this.builds) {
            if (place.tower == null)
                continue;
            
            for (let monster of monsters) {
                if (place.canAttack(monster)) {
                    place.attack(monster);
                    break;
                }
            }
        }
    }
    
    /**
     * Callback when a place is clicked
     */
    handleClick(event) {
        // stop propagation and hide active menu
        event.stopPropagation();
        this.hideActiveMenu();
        
        let place = event.currentTarget;
        
        // if user click on the same place
        if (this.selectedPlace == place) {
            this.selectedPlace = null;
            return;
        }
        
        this.selectedPlace = place;
        
        if (place.tower == null) {
            this.activeMenu = this.buildMenu;
        } else {
            this.activeMenu = this.createUpgradeMenu(place.tower);
            place.select();
        }
        
        this.showActiveMenu(place);
    }
    
    /**
     * Callback when user want to buy a tower
     */
    handleBuildClick(event, data) {
        
        let place = this.selectedPlace;
        let tower = data.tower;
        
        // stop propagation and hide active menu
        event.stopPropagation();
        this.hideActiveMenu();
        
        // build tower on place
        if (tower.price <= this.levelData.gold) {
            place.setTower(tower);
            this.levelData.gold -= tower.price;
        }
        
        this.handleTowerMouseOut(event);
        this.selectedPlace = null;
    }
    
    /** 
     * Callback when user want to sell a tower
     */
    handleSellClick(event, data) {
        let place = this.selectedPlace;
        
        // stop propagation and hide active menu
        event.stopPropagation();
        this.hideActiveMenu();
        
        let towerValue = place.tower.price;
        
        // remove tower from place
        place.removeTower();
        this.levelData.gold += towerValue;
        
        this.selectedPlace = null;
    }
    
    /**
     * Callback when user target a buyable tower (show the range of the tower)
     */
    handleTowerMouseOver(event, data) {
        let place = this.selectedPlace;
        let tower = data.tower;
        
        if (place.tower)
            place.handleMouseOver(event);
        
        this.rangeTooltip = place.createRangeTooltip(tower.range, "#228925");
        this.rangeTooltip.visible = true;
        this.addChildAt(this.rangeTooltip, 0);
    }
    
    /**
     * Callback when user stop targetting a buyable tower (hide the range of the tower)
     */
    handleTowerMouseOut(event) {
        let place = this.selectedPlace;
        
        if (place != null && place.tower)
            place.handleMouseOut(event);
        
        this.removeChild(this.rangeTooltip);
    }
}