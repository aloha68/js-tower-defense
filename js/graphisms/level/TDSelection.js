/**
 * Selection: contains selection data
 */
class TDSelection extends TDLevelContainer {
    constructor(level, x, y, width, height) {
        super(level);
        
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.setBounds(x, y, width, height);
        
        this.currentX = 10;
        this.init();
    }
    
    /**
     * Init the selection area
     */
    init() {
        // create border of selection area
        let border = new createjs.Shape();
        border.graphics
            .setStrokeStyle(1)
            .beginStroke("#777")
            .drawRoundRect(0, 0, this.width, this.height, 2);
        border.alpha = 0.8;
        
        this.addChild(border);
    }
    
    /**
     * Clear the current selection
     */
    clear() {
        this.currentX = 10;
        let numChildren = this.numChildren;
        for (let i = numChildren-1; i > 0; i--) {
            this.removeChildAt(i);
        }
    }
    
    /**
     * Add a space in the current selection
     */
    addSpace(size) {
        if (size > 0)
            this.currentX += size;
        return this;
    }
    
    /**
     * Add an icon to selection
     * We consider there is a image which is called "hud" + image name
     */
    addIcon(imageName) {
        let icon = this.createIcon("hud" + imageName, 16, "W");
        icon.x = this.currentX;
        icon.y = this.height / 2 - 2;
        this.addChild(icon);
        
        this.currentX += 20;
        return this;
    }
    
    /**
     * Add a text to the current selection
     */
    addText(data, font="16px JWerd", color="") {
        let text = this.createText(data, "W", font, color);
        text.x = this.currentX;
        text.y = this.height / 2;
        this.addChild(text);
        
        this.currentX += text.getMeasuredWidth();
        return this;
    }
}