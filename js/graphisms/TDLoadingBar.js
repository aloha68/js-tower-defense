/**
 * Filling bar from 0 to 100
 */
class TDLoadingBar extends TDContainer {
    constructor(game, width, height, fillColor="red", backColor="black") {
        super(game);
        
        this.width = width;
        this.height = height;
        this.fillColor = fillColor;
        this.backColor = backColor;
        
        this.loadingBar = null;
        this.loadingBarFrame = null;
        
        this.init();
    }
    
    /**
     * Init the loading bar
     */
    init() {
        this.loadingBarFrame = new createjs.Shape()
        this.loadingBarFrame.graphics
            .setStrokeStyle(1)
            .beginStroke(this.backColor)
            .beginFill(this.backColor)
            .drawRect(0, 0, this.width, this.height)
            .endFill()
            .endStroke();
        this.addChild(this.loadingBarFrame);
        
        // create health bar
        this.loadingBar = new createjs.Shape();
        this.loadingBar.graphics
            .beginFill(this.fillColor)
            .drawRect(0, 0, this.width / 100, this.height)
            .endFill();
        this.addChild(this.loadingBar);
    }
    
    /**
     * Update the percentage of the loading bar
     */
    updatePercent(percent) {
        if (percent < 0 || percent > 100)
            throw "TDLoadingBar: Impossible to update percent to that value: " + percent;
        this.loadingBar.scaleX = percent;
    }
}