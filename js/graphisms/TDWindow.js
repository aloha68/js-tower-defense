/**
 * LevelWindow: a window that appears on level screen
 */
class TDWindow extends TDContainer {
    constructor(level, width, height, title) {
        super(level);
        
        this.width = width;
        this.height = height;
        this.title = title;
    }
    
    init() {
        this.drawBackground();
        this.drawTitle();
    }
    
    /**
     * Draw the window background
     */
    drawBackground() {
        let background = new createjs.Shape();
        background.graphics
            .setStrokeStyle(1)
            .beginStroke("#777")
            .beginFill("#eee")
            .drawRoundRect(0, 0, this.width, this.height, 3);
        
        this.addChild(background);
    }
    
    /**
     * Draw the window title
     */
    drawTitle() {
        let title = this.title.toUpperCase();
        let text = this.createText(title, "N", "30px JWerd", "black");
        text.x = this.width / 2;
        text.y = 15;
        this.addChild(text);
        
        // add a line under the title
        let line = new createjs.Shape();
        line.graphics
            .beginFill("#000")
            .drawRect(0, 50, this.width, 3);
        this.addChild(line);
    }
}