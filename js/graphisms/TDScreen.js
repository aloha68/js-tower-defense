/**
 * Custom class to handle entire canvas
 */
class TDScreen extends TDContainer {
    constructor(game) {
        super(game);
        this.canvas = this.game.canvas;
        this.setBounds(0, 0, this.canvas.width, this.canvas.height);
    }
}