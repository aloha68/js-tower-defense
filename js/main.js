let game = null;

function init() {
    
    // Throw an error if the game is already launched
    if (game !== null)
        throw "Game is already launched.";
    
    // Get the game title element
    let title = document.getElementById(GAME_TITLE_ELEMENT);
    if (title === null)
        throw `Game title element not found: ${GAME_TITLE_ELEMENT}.`;
    
    document.title = GAME_NAME;
    title.innerHTML = GAME_NAME;
    
    // Get the game canvas element
    let canvas = document.getElementById(GAME_CANVAS_ELEMENT);
    if (canvas === null)
        throw `Game canvas element not found: ${GAME_CANVAS_ELEMENT}.`;
    
    canvas.width = GAME_WIDTH;
    canvas.height = GAME_HEIGHT;
    
    game = new Game(canvas);
    game.start();
}