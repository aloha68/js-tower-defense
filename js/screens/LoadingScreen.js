/**
 * Loading screen
 */
class LoadingScreen extends TDScreen {
    constructor(game) {
        super(game);
        
        this.loadingBar = null;
        this.statusText = null;
        
        this.init();
    }
    
    /**
     * Initialize the loading screen
     */
    init() {
        this.name = "loadingScreen";
        
        // we can't use image name here because files are not loaded
        let homeImage = new createjs.Bitmap('images/loading-screen.png');
        this.addChild(homeImage);
        
        // loading bar
        this.loadingBar = new TDLoadingBar(this.game, LOADING_BAR_WIDTH, LOADING_BAR_HEIGHT, LOADING_BAR_COLOR, LOADING_BAR_BACKCOLOR);
        this.loadingBar.x = LOADING_BAR_X;
        this.loadingBar.y = LOADING_BAR_Y;
        this.addChild(this.loadingBar);
        
        // status bar
        this.statusText = this.createText("", "S", "18px JWerd", "black");
        this.statusText.x = LOADING_BAR_X + LOADING_BAR_WIDTH / 2;
        this.statusText.y = LOADING_BAR_Y + LOADING_BAR_HEIGHT / 2;
        this.addChild(this.statusText);
        
        // init screen
        this.updateStatus("Loading...", 0);
    }
    
    /**
     * Start the loading of all game data
     */
    loadData() {
        console.log(`${this.constructor.name}.loadData()`);
        
        let dataLoading = new DataLoading();
        dataLoading.on("progress", this.handleDataProgress, this);
        dataLoading.on("ready", this.handleDataReady, this);
        dataLoading.start();
    }
    
    /**
     * Update the status of the current loading
     */
    updateStatus(text, percent=0) {
        this.statusText.text = text;
        this.statusText.regX = this.statusText.getMeasuredWidth() / 2;
        this.statusText.regY = this.statusText.getMeasuredHeight() / 2;
        
        if (percent >= 0) {
            this.loadingBar.updatePercent(percent);
        }
    }
    
    /**
     * Callback when data loading progress
     */
    handleDataProgress(event) {
        let details = event.details;
        this.updateStatus(details.status, details.percent);
    }
    
    /**
     * Callback when data loading is finished
     */
    handleDataReady(event) {
        console.log(`${this.constructor.name}.handleDataReady()`);
        
        let finishEvent = new createjs.Event("finish");
        finishEvent.data = event.data;
        finishEvent.loadQueue = event.loadQueue;
        this.dispatchEvent(finishEvent);
    }
}