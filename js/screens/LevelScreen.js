/**
 * LevelScreen: class to handle a full level
 */
class LevelScreen extends TDScreen {
    constructor(game, level, tilesetFile) {
        super(game);
        
        this.levelData = level;
        this.tilesetFile = tilesetFile;
        
        this.mapData = [];
        this.entrance = [0, 0];
        this.exit = [0, 0];
        this.path = null;
        this.levelComplete = false;
        
        this.window = null;
        this.windowBackground = null;
        
        this.cntBackground = null; // init on init()
        this.cntBuilds = null; // init on init()
        this.cntHud = null; // init on init()
        
        this.wave = null; // init on start()
        
        this.init();
    }
    
    /**
     * Init the level screen
     */
    init() {
        console.log(`${this.constructor.name}.init()`);
        console.log(`  Level: ${this.levelData.num}, ${this.levelData.numWaves} wave(s)`);
        console.log(`  Tileset file: ${this.tilesetFile}`);
        
        // Some Container properties
        this.name = "cntLevel";
        
        // Load JSON files
        let tilesetJson = this.getFile(this.tilesetFile);
        
        // Background
        this.cntBackground = new TDBackground(this, tilesetJson);
        this.cntBackground.on("ready", this.handleBackgroundReady, this);
        
        // HUD
        this.cntHud = new TDHud(this);
        this.cntHud.on("pause", this.handlePause, this);
        this.cntHud.on("startWave", this.handleWaveStarted, this);
        
        // Build places
        this.cntBuilds = new TDBuilds(this);
        
        // Draw all tiles layers
        let levelTilesJson = this.getFile("data/level" + this.levelData.num + "-tiles.json");
        this.drawLayers(levelTilesJson.layers);
    }
    
    /**
     * Draw building places from BuildingPlaces layer
     */
    drawBuildingPlaces(layerObjects) {
        for (let i = 0; i < layerObjects.length; i += 1) {
            let obj = layerObjects[i];
            this.cntBuilds.addBuildingPlace(obj.x, obj.y);
        }
    }
    
    /**
     * Search entrance and exit from EntrancesExits layer
     */
    searchEntranceAndExit(layerObjects) {
        for (let objIndex = 0; objIndex < layerObjects.length; objIndex += 1) {
            let obj = layerObjects[objIndex];
            let objX = Math.floor(obj.x / GAME_TILE_SIZE),
                objY = Math.floor(obj.y / GAME_TILE_SIZE);
                
            if (obj.type == "entrance") {
                this.entrance = [objX, objY];
            } else if (obj.type == "exit") {
                this.exit = [objX, objY];
            }
        }
    }
    
    /**
     * Draw all layers
     */
    drawLayers(layers) {
        for (let i = 0; i < layers.length; i++) {
            let layer = layers[i];
            
            if (layer.type == 'tilelayer') {
                this.cntBackground.addLayer(layer);
            } else if (layer.type == 'objectgroup') {
                if (layer.name == "EntrancesExits") {
                    this.searchEntranceAndExit(layer.objects);
                } else if (layer.name == "BuildingPlaces") {
                    this.drawBuildingPlaces(layer.objects);
                }
            }
        }
        
        this.cntBackground.drawAllLayers();
    }
    
    /**
     * Callback when background is ready
     */
    handleBackgroundReady(event) {
        this.logMethod("handleBackgroundReady");
        this.mapData = event.mapData;
        this.calculatePath();
    }
    
    /**
     * Calculate the path of the current level
     */
    calculatePath(callback) {
        this.logMethod("calculatePath");
        
        let easystar = new EasyStar.js();
        easystar.setGrid(this.mapData);
        easystar.setAcceptableTiles([1]);
        
        let entranceX = this.entrance[0],
            entranceY = this.entrance[1],
            exitX = this.exit[0],
            exitY = this.exit[1]; 
        easystar.findPath(entranceX, entranceY, exitX, exitY, 
            (path) => this.handlePathCalculated(path));
        
        easystar.calculate();
    }
    
    /**
     * Callback when the path is calculated
     */ 
    handlePathCalculated(path) {
        if (path === null) throw "Path can not be calculate...";
        this.logMethod("handlePathCalculated");
        this.path = path;
        this.start();
    }
    
    /**
     * Start the level
     */
    start() {
        if (!this.cntBackground.isReady)
            return;
        
        this.logMethod("start");
        
        // Wave
        this.wave = new Wave(this.path);
        this.wave.on('finish', this.handleWaveFinished, this);
        
        // Add all layers
        this.addChild(this.cntBackground);
        this.addChild(this.cntHud);
        this.addChild(this.cntBuilds);
        this.addChild(this.wave.graphics);
        
        this.showStartWaveButton();
        this.game.paused = true;
        
        this.on("click", this.handleClick);
        this.on("tick", this.handleTick);
    }
    
    /**
     * Select an object
     */
    selectObject(obj) {
        if (obj instanceof TDLevelSelectableContainer)
            this.cntHud.selectedTarget = obj;
    }
    
    /**
     * Clear the current selection
     */
    clearSelection() {
        this.cntHud.selectedTarget = null;
    }
    
    /**
     * Show the button to start the next wave
     */
    showStartWaveButton() {
        let entranceX = this.entrance[0] * GAME_TILE_SIZE,
            entranceY = this.entrance[1] * GAME_TILE_SIZE;
        this.cntHud.showStartWaveButton(entranceX, entranceY);
    }
    
    /**
     * Callback when a wave is started
     */
    handleWaveStarted(event) {
        this.levelData.numWave += 1;
        this.logMethod("handleWaveStart", this.levelData.numWave);
		
        this.game.paused = false;
        this.wave.startWave(this.levelData.currentWave);
	}
    
    /**
     * Callback when a wave is finished
     */
    handleWaveFinished(event) {
        this.logMethod("handleWaveFinish", this.levelData.numWave);
        this.game.paused = true;
        
        this.cntHud.updateStatus("Wave " + this.levelData.numWave + " finished");
        
        // if level is finished
        if (this.levelData.numWave == this.levelData.numWaves) {
            this.levelComplete = true;
            this.showEndLevelWindow();
            return;
        }

        this.showStartWaveButton();
	}
	
    /**
     * Callback when next level is called
     */
	handleNextLevel(event) {
        this.btnStartLevel.visible = false;
        this.dispatchEvent(new createjs.Event("finish"));
	}
    
    /**
     * Returns the black background layer used to display a window
     */
    getWindowBackground() {
        if (this.windowBackground !== null)
            return this.windowBackground;
        
        this.windowBackground = new createjs.Shape();
        this.windowBackground.graphics
            .beginFill("#000")
            .drawRect(0, 0, this.canvas.width, this.canvas.height);
        this.windowBackground.alpha = 0.5;
        
        return this.windowBackground;
    }
    
    /**
     * Show the given window
     */
    showWindow(window) {
        this.window = window;
        
        // window positioning
        this.window.regX = this.window.width / 2;
        this.window.regY = this.window.height / 2;
        this.window.x = this.game.canvas.width / 2;
        this.window.y = this.game.canvas.height / 2;
        
        // black background
        let background = this.getWindowBackground();
        
        this.addChild(background);
        this.addChild(window);
    }
    
    /**
     * Hide the current window
     */
    hideCurrentWindow() {
        if (this.window === null)
            return;
        
        this.removeChild(this.window);
        this.window = null;
        
        if (this.windowBackground !== null)
            this.removeChild(this.windowBackground);
    }
    
    /**
     * Show the pause window
     */
    showPauseWindow() {
        let window = new TDPauseWindow(this);
        window.on("resume", this.handleResume, this);
        window.on("restart", this.handleRestartLevel, this);
        window.on("menu", this.handleReturnMenu, this);
        this.showWindow(window);
    }
    
    /**
     * Show the success end window
     */
    showEndLevelWindow() {
        let window = new TDEndLevelWindow(this, this.levelComplete);
        window.on("restart", this.handleRestartLevel, this);
        window.on("menu", this.handleReturnMenu, this);
        this.showWindow(window);
    }
    
    /*
     * Callback when game is paused
     */
    handlePause(event) {
        this.game.paused = true;
        this.showPauseWindow();
    }
    
    /**
     * Callback when game is resumed
     */
    handleResume(event) {
        if (this.levelData.numWave > 0)
            this.game.paused = false;
        this.hideCurrentWindow();
    }
    
    /**
     * Callback when the level is restarted
     */
    handleRestartLevel(event) {
        let ev = new createjs.Event("restart");
        ev.levelComplete = this.levelComplete;
        this.dispatchEvent(ev);
    }
    
    /**
     * Callback when user ask to come back to menu
     */
    handleReturnMenu(event) {
        let ev = new createjs.Event("finish");
        ev.levelComplete = this.levelComplete;
        this.dispatchEvent(ev);
    }
    
    /**
     * Handle click
     */
    handleClick(event) {
        this.logMethod("handleClick", this.name);
        this.cntBuilds.hideActiveMenu();
        this.clearSelection();
    }
    
    /**
     * Callback on tick (to call towers attack)
     */ 
    handleTick(event) {
        if (this.game.paused)
            return;
        
        // if game is over
        if (this.levelData.lifes <= 0) {
            this.game.paused = true;
            this.showEndLevelWindow();
            return;
        }
        
        // check if attack is possible
        this.cntBuilds.checkAttack(this.wave.monsters);
    }
}