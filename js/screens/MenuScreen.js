/**
 * A menu entry for levels
 */
class MenuLevelEntry extends TDContainer {
    constructor(game, level) {
        super(game);
        this.level = level;
        this.init();
    }
    
    init() {
        this.cursor = "pointer";
        
        let waveText = this.level.numWaves > 1 ? "waves" : "wave";
        let levelText = `${this.level.name} (${this.level.numWaves} ${waveText})`;
        let text = this.createText(levelText, "W", "18px JWerd", "black");
        text.x = 20;
        text.y = this.currentY;
        
        let rectX = -10,
            rectY = -10,
            rectWidth = text.getMeasuredWidth() + 20,
            rectHeight = text.getMeasuredHeight() + 20,
            strokeThickness = 1;
        
        let rect = new createjs.Shape();
        rect.graphics
            .setStrokeStyle(strokeThickness)
            .beginStroke("#777")
            .beginFill("#eee")
            .drawRoundRect(rectX, rectY, rectWidth, rectHeight, 3);
        rect.alpha = 0.7;
        this.addChild(rect);
        
        this.addChild(text);
    }
}

/**
 * Menu screen
 */
class MenuScreen extends TDScreen {
    constructor(game) {
        super(game);
        this.currentY = 100;
        this.init();
    }
    
    /**
     * Initialize the menu screen
     */
    init() {
        this.name = "menuScreen";
        
        let background = new createjs.Bitmap(this.getFile('menuBackground'));
        this.addChild(background);
        
        // status bar
        this.menuTitle = this.createText("Menu !", "S", "48px JWerd", "#DDD");
        this.menuTitle.x = this.canvas.width / 2;
        this.menuTitle.y = 50;
        this.addChild(this.menuTitle);
    }
    
    /**
     * Reset the menu screen
     */
    reset() {
        for (let child of this.children)
            child.removeAllEventListeners();
        this.removeAllChildren();
        this.currentY = 100;
        this.init();
    }
    
    /**
     * Add a level to the menu screen
     */
    addLevel(level) {
        let eventData = {level: level};
        let entry = new MenuLevelEntry(this.game, level);
        entry.x = 70;
        entry.y = this.currentY;
        entry.on("click", this.handleLevelClick, this, false, eventData);
        this.addChild(entry);
        
        this.currentY += 50;
    }
    
    /**
     * Callback when a level is clicked
     */
    handleLevelClick(event, data) {
        let levelSelectedEvent = new createjs.Event("levelClick");
        levelSelectedEvent.level = data.level;
        this.dispatchEvent(levelSelectedEvent);
    }
}