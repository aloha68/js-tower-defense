/**
 * GameData: download and read or preload all game data files
 */
class GameData extends createjs.EventDispatcher {
    constructor() {
        super();
        
        this.files = [];
        this.images = {};
        
        this.towers = [];
        this.monsters = {};
        this.levels = {};
    }
}