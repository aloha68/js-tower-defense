/**
 * DataLoading: manage data game loading
 */
class DataLoading extends createjs.EventDispatcher {
    constructor() {
        super();
        
        this.files = [
            "data/data.json",
            "data/monsters.json",
            "data/towers.json"
        ];
        
        this.data = new GameData();
        this.nbFilesDownloaded = 0;
        this.loadQueue = null;
        this.isComplete = false;
    }
    
    /**
     * Start the loading of all data
     */
    start() {
        console.log(`${this.constructor.name}.start()`);
        
        this.loadQueue = new createjs.LoadQueue();
        this.loadQueue.on("progress", this.handleLoadProgress, this);
        this.loadQueue.on("complete", this.handleLoadComplete, this);
        
        for (let file of this.files) {
            this.downloadFile(file);
        }
    }
    
    /**
     * Download a file with a XMLHttpRequest
     */
    downloadFile(file) {
        console.log(`${this.constructor.name}.downloadFile("${file}")`);
        this.sendProgressEvent("Downloading: " + file);
        
        let xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open('GET', file, true);
        xobj.onreadystatechange = (e) => this.handleFileDownloaded(e);
        xobj.send(null);
    }
    
    /**
     * Callback when a file is downloaded
     */
    handleFileDownloaded(event) {
        let request = event.currentTarget;
        if (request.readyState == 4 && request.status == "200") {
            
            let json = JSON.parse(request.responseText);
            if (request.responseURL.endsWith("data.json")) {
                this.parseDataFile(json);
            } else if (request.responseURL.endsWith("monsters.json")) {
                this.parseMonstersFile(json);
            } else if (request.responseURL.endsWith("towers.json")) {
                this.parseTowersFile(json);
            } else {
                throw new DataLoadingException(`File ${request.responseURL} not handled`);
            }
            
            this.nbFilesDownloaded += 1;
            if (this.nbFilesDownloaded == this.files.length) {
                this.launchQueue();
            }
        }
    }
    
    /**
     * Parse game data file
     */
    parseDataFile(data) {
        console.log(`${this.constructor.name}.parseDataFile()`);
        
        this.data.files = data.files;
        for (let key of Object.keys(data.images))
            this.data.images[key] = data.images[key];
    }
    
    /** 
     * Parse monsters file
     */
    parseMonstersFile(data) {
        console.log(`${this.constructor.name}.parseMonstersFile()`);
        
        this.data.monsters = data.Monsters;
        
        // parse all monsters images 
        for (let key of Object.keys(this.data.monsters)) {
            let monster = this.data.monsters[key];
            let i = 0;
            for (let image of monster.spriteSheet.images) {
                this.data.images[monster.name + i] = image;
                i += 1;
            }
        }
    }
    
    /**
     * Parse JSON tower data and add images to files to load
     */
    parseTowerData(tower) {
        if (tower == null) return;    
        this.data.images[tower.id] = tower.icon;
        
        // looking for upgrades
        if (!tower.hasOwnProperty("upgradableTowers")) {
            tower.upgradableTowers = [];
        } else {
            for (let i = 0; i < tower.upgradableTowers.length; i++) {
                this.parseTowerData(tower.upgradableTowers[i]);
            }
        }
        return tower;
    }
    
    /**
     * Parse towers file
     */
    parseTowersFile(data) {
        console.log(`${this.constructor.name}.parseTowersFile()`);
        
        // load towers
        for (let i = 0; i < data.Towers.length; i++) {
            this.data.towers.push(this.parseTowerData(data.Towers[i]));
        }
    }
    
    /**
     * Parse level file
     */
    parseLevelFile(levelNumber, data) {
        console.log(`${this.constructor.name}.parseLevelFile(${levelNumber})`);
        
        let level = new LevelData();
        level.init(levelNumber, data.name, data.gold, data.lifes, data.waves);
        
        this.data.levels[levelNumber] = level;
    }
    
    /**
     * Parse all level files
     */
    parseLevelFiles() {
        console.log(`${this.constructor.name}.parseLevelFiles()`);
        
        let levelNumber = 1;
        while (true) {
            let dataFile = this.getFile("data/level" + levelNumber + "-data.json", false);
            let tilesFile = this.getFile("data/level" + levelNumber + "-tiles.json", false);
            
            if (dataFile === null || tilesFile == null)
                break;
            
            this.parseLevelFile(levelNumber, dataFile);
            levelNumber += 1;
        }
    }
    
    /**
     * Launch PreloadJS queue for all others files
     */
    launchQueue() {
        console.log(`${this.constructor.name}.launchQueue()`);
        
        for (let file of this.data.files) {
            this.loadQueue.loadFile(file);
        }
        
        for (let key of Object.keys(this.data.images)) {
            this.loadQueue.loadFile({id: key, src: this.data.images[key]});
        }
    }
    
    /**
     * Get a file from the load queue
     */
    getFile(fileName, throwIfFileDoesntExist=true) {
        if (!this.isComplete)
            throw new DataLoadingException("Can't get file while loading isn't complete");
        
        let file = this.loadQueue.getResult(fileName);
        if (file === null && throwIfFileDoesntExist)
            throw new DataLoadingException("Could not find file: " + fileName);
        
        return file;
    }
    
    /**
     * Send progress event to listeners
     */
    sendProgressEvent(status, percent=0) {
        let event = new createjs.Event("progress");
        event.details = {};
        event.details.status = status;
        event.details.percent = percent;
        this.dispatchEvent(event);
    }
    
    /**
     * Send ready event to listeners
     */
    sendReadyEvent() {
        let event = new createjs.Event("ready");
        event.data = this.data;
        event.loadQueue = this.loadQueue;
        this.dispatchEvent(event);
    }
    
    /**
     * Handle preload progress
     */
    handleLoadProgress(event) {
        let percent = Math.round(event.progress * 100);
        this.sendProgressEvent("Loading files: " + percent + "%", percent);
    }
    
    /**
     * Handle preload complete
     */
    handleLoadComplete(event) {
        console.log(`${this.constructor.name}.handleLoadComplete()`);
        this.isComplete = true;
        this.parseLevelFiles();
        this.sendReadyEvent();
    }
}