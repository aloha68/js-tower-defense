/**
 * LevelData: contains all data for a level
 */
class LevelData extends createjs.EventDispatcher {
    constructor() {
        super();
        
        this._num = 0;
        this._name = "";
        
        this._gold = 0;
        this._lifes = 0;
        this._isLocked = false;
        
        this._waves = [];
        this._numWave = 0;
    }
    
    /**
     * Initialize level data
     */
    init(num, name, gold, lifes, waves) {
        this._num = num;
        this._name = name;
        this._gold = gold;
        this._lifes = lifes;
        this._waves = waves;
        this._isLocked = num > 1;
    }
    
    /**
     * Clone the level data to a new object
     */
    copy() {
        let levelData = new LevelData();
        
        // copy waves
        let waves = [];
        for (let oldWave of this.waves) {
            
            let wave = Object.assign({}, oldWave);
            
            // copy monsters
            wave.monsters = [];
            for (let oldMonster of oldWave.monsters) {
                let monster = Object.assign({}, oldMonster);
                wave.monsters.push(monster);
            }
            
            waves.push(wave);
        }
        
        levelData.init(this.num, this.name, this.gold, this.lifes, waves);
        return levelData;
    }
    
    /**
     * Send a changed event to all listeners
     */
    sendChangedEvent() {
        let event = new createjs.Event("change");
        this.dispatchEvent(event);
    }
    
    get num() {
        return this._num;
    }
    
    get name() {
        return this._name;
    }
    
    get gold() {
        return this._gold;
    }
    
    set gold(value) {
        this._gold = value;
        this.sendChangedEvent();
    }
    
    get lifes() {
        return this._lifes;
    }
    
    set lifes(value) {
        this._lifes = value;
        this.sendChangedEvent();
    }
    
    get isLocked() {
        return this._isLocked;
    }
    
    set isLocked(value) {
        this._isLocked = value;
    }
    
    get waves() {
        return this._waves;
    }
    
    get numWaves() {
        return this._waves.length;
    }
    
    get numWave() {
        return this._numWave;
    }
    
    set numWave(value) {
        this._numWave = value;
        this.sendChangedEvent();
    }
    
    get currentWave() {
        return this._waves[this._numWave-1];
    }
}