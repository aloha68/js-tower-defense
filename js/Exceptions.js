/**
 * Generic exception
 */
class TDException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, TDException);
        }
    }
    
    toString() {
        return this.name + ": " + this.message;
    }
}

/**
 * Exception raised when the game is not ready
 */
class GameNotReadyException extends TDException { }

/**
 * Exception raised when an issue appear during data loading
 */
class DataLoadingException extends TDException { }
