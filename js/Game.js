/**
 * Game: It's the main class of this game! Hi there! :-)
 */
class Game extends createjs.Stage {
    
    constructor(canvas) {
        super(canvas);
        
        this.data = null;
        this.loadQueue = null;
        
        this.isReady = false;
        this.isStarted = false;
        this.currentLevel = null;
        
        this.loadingScreen = null;
        this.menuScreen = null;
        this.levelScreen = null;
        this.tilesetFile = "data/tileset.json"
        
        this.init();
    }
    
    /**
     * Return true if game is paused
     */
    get paused() {
        return createjs.Ticker.paused;
    }
    
    /**
     * Set the game paused or unpaused
     */
    set paused(value) {
        if (typeof value !== 'boolean')
            throw new Error("Can't set a non-boolean value as Game.paused");
        
        if (value != this.paused) {
            console.log(`Game.paused = ${value}`);
            createjs.Ticker.paused = value;
        }
    }
    
    /**
     * Init the game
     */
    init() {
        console.log("Game.init()");
        
        // activate mouse support
        // parameter = number of update each seconds
        // a lower frequency is less responsive, but uses less CPU
        this.enableMouseOver(10);
    
        // create main ticker
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 30;
        createjs.Ticker.on("tick", this.handleTick, this);
        
        // show loading screen
        this.showLoadingScreen();
    }
    
    /**
     * Start the game, possible only when game is ready
     */
    start() {
        if (!this.isReady || this.isStarted) return;
        this.isStarted = true;
        console.log("Game.start()");
        
        utils.init();
        this.hideLoadingScreen();
        this.showMenuScreen();
    }
    
    /**
     * Get a preloaded file from its name or its path
     * @param fileName
     */
    getFile(fileName) {
        if (!this.isReady || this.loadQueue === null)
            throw new GameNotReadyException("Files are not ready.");
        
        let file = this.loadQueue.getResult(fileName);
        if (file === null) {
            throw "Could not find file: " + fileName;
        }
        return file;
    }
    
    /**
     * Show the loading screen
     */
    showLoadingScreen() {
        console.log("Game.showLoadingScreen()");
        
        this.loadingScreen = new LoadingScreen(this);
        this.loadingScreen.on("finish", this.handleLoadingFinish, this);
        this.addChild(this.loadingScreen);
        
        this.loadingScreen.loadData();
    }
    
    /**
     * Hide the loading screen
     */
    hideLoadingScreen() {
        console.log("Game.hideLoadingScreen()");
        this.loadingScreen.visible = false;
        
        // Delete loading screen because it will be not used anymore
        this.removeChild(this.loadingScreen);
        this.loadingScreen = null;
    }
    
    /**
     * Show the menu screen
     */
    showMenuScreen() {
        console.log("Game.showMenuScreen()");
        if (this.menuScreen === null) {
            this.menuScreen = new MenuScreen(this);
            this.menuScreen.on("levelClick", this.handleMenuLevelClick, this);
        }
        
        this.menuScreen.reset();
        for (let levelNumber of Object.keys(this.data.levels)) {
            let level = this.data.levels[levelNumber];
            if (!level.isLocked || DEBUG_UNLOCK_ALL_LEVELS)
                this.menuScreen.addLevel(level);
        }
        
        this.menuScreen.visible = true;
        this.addChild(this.menuScreen);
    }
    
    /**
     * Hide the menu screen
     */
    hideMenuScreen() {
        console.log("Game.hideMenuScreen()");
        this.menuScreen.visible = false;
    }
    
    /**
     * Start a new level
     */
    startLevel(level) {
        console.log(`Game.startLevel(${level.num})`);
        
        this.currentLevel = level;
        let levelCopy = level.copy();
        
        this.levelScreen = new LevelScreen(this, levelCopy, this.tilesetFile);
        this.levelScreen.on("finish", this.handleLevelFinish, this);
        this.levelScreen.on("restart", this.handleLevelRestart, this);
        this.showLevelScreen();
    }
    
    /**
     * Show the level screen
     */
    showLevelScreen() {
        console.log("Game.showLevelScreen()");
        this.levelScreen.visible = true;
        this.addChild(this.levelScreen);
    }
    
    /**
     * Hide the level screen
     */
    hideLevelScreen() {
        console.log("Game.hideLevelScreen()");
        this.currentLevel = null;
        
        this.levelScreen.removeAll();
        this.removeChild(this.levelScreen);
        this.levelScreen = null;
    }
    
    /**
     * Unlock the next level
     */
    unlockNextLevel() {
        let levelNumber = 0;
        if (this.currentLevel === null)
            levelNumber = 1;
        else
            levelNumber = this.currentLevel.num + 1;
        
        console.log(`Game.unlockNextLevel(${levelNumber})`);
        if (this.data.levels.hasOwnProperty(levelNumber))
            this.data.levels[levelNumber].isLocked = false;
    }
    
    /**
     * Callback when data are loaded
     */
    handleLoadingFinish(event) {
        console.log("Game.handleLoadingFinish()");
        this.isReady = true;
        
        this.data = event.data;
        this.loadQueue = event.loadQueue;
        
        let lastUpdate = this.getFile("last_update.txt");
        document.getElementById('spanLastUpdate').innerHTML = lastUpdate;
        
        let finalText = "Loading complete. Click anywhere to start.";
        this.loadingScreen.updateStatus(finalText, 100);
        
        if (!DEBUG || DEBUG_START_LEVEL === 0) {
            this.loadingScreen.on("click", this.start, this);
            
        } else {
            this.start();
            this.hideMenuScreen();
            this.startLevel(this.data.levels[DEBUG_START_LEVEL]);
        }
    }
    
    /**
     * Callback when a level is selected on the menu
     */
    handleMenuLevelClick(event) {
        console.log("Game.handleMenuLevelSelected()");
        this.hideMenuScreen();
        this.startLevel(event.level);
    }
    
    /**
     * Callback when a level is finished
     */
    handleLevelFinish(event) {
        console.log(`Game.handleLevelFinish(${this.currentLevel.num})`);
        
        if (event.levelComplete)
            this.unlockNextLevel();
        
        this.hideLevelScreen();
        this.showMenuScreen();
	}
    
    /**
     * Callback when a level is restarted
     */
    handleLevelRestart(event) {
        console.log(`Game.handleLevelRestart(${this.currentLevel.num})`);
        
        if (event.levelComplete)
            this.unlockNextLevel();
        
        let level = this.currentLevel;
        this.hideLevelScreen();
        this.startLevel(level);
    }
    
    /**
     * Handle main easelJs tick 
     */
    handleTick(event) {
        this.update(event);
        let fps = Math.round(createjs.Ticker.getMeasuredFPS());
        document.getElementById("spanFPS").innerHTML = fps;
    }
}
