/**
 * Utilities class about data
 */
class DataUtils {
    
    /**
     * Return the current level data
     */
    get currentLevel() {
        return game.levelScreen.levelData;
    }
}


/**
 * Utility class
 */
class Utils {
    
    constructor() {
        this.isInit = false;
        this.data = new DataUtils();
        this._monsterFactory = null;
        this.projectileFactory = new ProjectileFactory();
    }
    
    /**
     * Throw an error if Utils is not inited
     */
    _throwIfNotInited() {
        if (!this.isInit)
            throw new Error('Utils is not inited at the moment');
    }
    
    /**
     * Init all utilities
     */
    init() {
        if (this.isInit)
            throw new Error("Utils already init");
        
        this.logMethod(this, 'init');
        
        this.isInit = true;
        this._monsterFactory = new MonsterFactory(game.data.monsters);
    }
    
    /**
     * Return the monster factory
     */
    get MonsterFactory() {
        this._throwIfNotInited();
        return this._monsterFactory;
    }
    
    /**
     * Return a preloaded file
     */
    getFile(fileName) {
        this._throwIfNotInited();
        
        let file = game.loadQueue.getResult(fileName);
        if (file === null) {
            throw new Error("Could not find file: " + fileName);
        }
        return file;
    }
    
    /**
     * Log a message
     */
    log(message) {
        if (DEBUG)
            console.log(message);
    }
    
    /**
     * Log a method with args
     */
    logMethod(obj, methodName, args=null, message=null) {
        let msg = `${obj.constructor.name}.${methodName}(`;
        if (args !== null) {
            if (typeof args === 'string' && args.length > 0)
                msg += `"${args}"`;
            else
                msg += args;
        }
        msg += ')'
        if (message != null)
            msg += ': ' + message;
        this.log(msg);
    }
    
    /**
     * Log a message with property name
     */
    logProperty(obj, propertyName, value) {
        let msg = `${obj.constructor.name}.${propertyName}`;
        
        if (value === null || !Array.isArray(value) && typeof value !== 'object') {
            msg += ` = ${value}`;
            this.log(msg);
        } else {
            msg += " has a new value!";
            this.log(msg);
            this.log(value);
        }
    }
}

let utils = new Utils();