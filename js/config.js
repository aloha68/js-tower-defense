/*
 * GLOBAL VARIABLES
 */

/*** Debugging configuration ***/
const DEBUG = true;
const DEBUG_START_LEVEL = 1;
const DEBUG_UNLOCK_ALL_LEVELS = false;


const MENU_SIZE = 120;


/*** Game configuration ***/
const GAME_NAME = "JS Tower Defense";
const GAME_WIDTH = 800;
const GAME_HEIGHT = 600;
const GAME_TILE_SIZE = 16;

const GAME_TITLE_ELEMENT = 'game-title';
const GAME_CANVAS_ELEMENT = 'game-canvas';


/*** Loading screen configuration ***/
const LOADING_BAR_X = 100;
const LOADING_BAR_Y = 500;
const LOADING_BAR_WIDTH = 600;
const LOADING_BAR_HEIGHT = 70;
const LOADING_BAR_COLOR = "#F8D430";
const LOADING_BAR_BACKCOLOR = "transparent";


/*** Tooltip configuration ***/
const TOOLTIP_ELEMENT = 'tooltip';
const TOOLTIP_TITLE_ELEMENT = 'tooltip-title';
const TOOLTIP_BODY_ELEMENT = 'tooltip-body';

const TOOLTIP_BACKGROUND_COLOR = '#EEE';
const TOOLTIP_BORDER_COLOR = '#222';
const TOOLTIP_FONT_COLOR = '#222';
