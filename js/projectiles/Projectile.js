/**
 * Projectile: a tower projectile
 */
class Projectile extends createjs.EventDispatcher {
    
    constructor(target, movement, graphics) {
        super();
        
        this.target = target;
        this.movement = movement;
        this.graphics = graphics;
        
        this.speed = 10;
        this.isHit = false;
        
        this.tickListener = null;
        this.monsterDieListener = null
        this.monsterSurviveListener = null;
        
        this.init();
    }
    
    /**
     * Send the hit event
     */
    _sendHitEvent() {
        this.dispatchEvent(new createjs.Event('hit'));
    }
    
    /**
     * Send the miss event
     */
    _sendMissEvent() {
        this.dispatchEvent(new createjs.Event('miss'));
    }
    
    /**
     * Remove all listeners
     */
    _removeListeners() {
        this.graphics.off('tick', this.tickListener);
        this.target.off('die', this.monsterDieListener);
        this.target.off('survive', this.monsterSurviveListener);
        
        this.tickListener = null;
        this.monsterDieListener = null
        this.monsterSurviveListener = null;
    }
    
    /**
     * Init the projectile
     */
    init() {
        this.movement.init();
        this.graphics.init();
        
        this.tickListener = this.graphics.on('tick', this.handleTick, this);
        this.monsterDieListener = this.target.on('die', this.handleTargetRemoved, this);
        this.monsterSurviveListener = this.target.on('survive', this.handleTargetRemoved, this);
    }
    
    set x(value) {
        this.graphics.x = value;
    }
    
    get x() {
        return this.graphics.x;
    }
    
    set y(value) {
        this.graphics.y = value;
    }
    
    get y() {
        return this.graphics.y;
    }
    
    /**
     * Callback when the target die or survive
     */
    handleTargetRemoved(event) {
        this._removeListeners();
        this._sendMissEvent();
    }
    
    /**
     * Callback on the game tick
     */
    handleTick(event) {
        if (game.paused)
            return;
        
        this.movement.update(this);
        this.graphics.update(this);
        
        if (this.isHit) {
            this._removeListeners();
            this._sendHitEvent();
        }
    }
}