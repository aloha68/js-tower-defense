/**
 * ProjectileFactory:
 */
class ProjectileFactory {
    constructor() {
        this._movement = new ProjectileMovement();
    }
    
    createProjectile(target, x, y) {
        
        let graphics = new ProjectileGraphics();
        let proj = new Projectile(target, this._movement, graphics);
        
        proj.x = x;
        proj.y = y;
        
        return proj;
    }
}