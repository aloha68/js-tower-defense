/**
 * ProjectileGraphics: handle projectile graphisms
 */
class ProjectileGraphics extends createjs.Shape {
    constructor() {
        super();
    }
    
    /**
     * Init projectile graphisms
     */
    init() {
        this.graphics.beginFill("red").drawCircle(0, 0, 3);
    }
    
    /**
     * Update the projectile graphisms
     */
    update() {
        
    }
}