/**
 * ProjectileMovement: handle projectile movement
 */
class ProjectileMovement {
    
    /**
     * Init the projectile movement
     */
    init() {
        
    }
    
    /**
     * Update the projectile movement
     */
    update(proj) {
        
        let position = proj.target.getPosition(),
            dx = position.x - proj.x,
            dy = position.y - proj.y;
        
        if (Math.abs(dx) < proj.speed && Math.abs(dy) < proj.speed)
            proj.isHit = true;
        
        let length = Math.sqrt(dx * dx + dy * dy);
        dx = dx / length * proj.speed;
        dy = dy / length * proj.speed;

        proj.x += dx;
        proj.y += dy;
    }
}