/**
 * MonsterCollection: handle monsters collection
 */
class MonsterCollection {
    constructor() {
        this.monsters = {};
        
        this.length = 0;
        this.maxLength = 0;
        this.nbMonstersReleased = 0;
        this.nbMonstersRemoved = 0;
    }
    
    /**
     * Throw an error if the given parameter is not a monster
     */
    _throwIfNotMonster(monster) {
        if (!(monster instanceof Monster))
            throw new Error("Monster required: " + monster);
    }
    
    /**
     * Add the given monster to the collection
     */
    add(monster) {
        this._throwIfNotMonster(monster);
        
        let index = this.length;
        monster.__collectionIndex = index;
        this.monsters[index] = monster;
        
        this.length += 1;
        this.maxLength += 1;
    }
    
    /**
     * Remove the given monster from the collection
     */
    remove(monster) {
        this._throwIfNotMonster(monster);
        
        let index = monster.__collectionIndex;
        this.monsters[index] = null;
        delete this.monsters[index];
        
        this.length -= 1;
        this.nbMonstersRemoved += 1;
    }
    
    /**
     * Release all monsters below delay
     */
    releaseMonsters(delay) {
        if (delay <= 0 || this.isEverythingReleased())
            return;
        
        let monsters = [];
        
        for (let monster of Object.values(this.monsters)) {
            if (monster.delay <= delay && !monster.hasOwnProperty('__isReleased')) {
                monster.__isReleased = true;
                monsters.push(monster);
            }
        }
        
        this.nbMonstersReleased += monsters.length;
        return monsters;
    }
    
    /**
     * Return true if every monsters are released
     */
    isEverythingReleased() {
        return this.maxLength == this.nbMonstersReleased;
    }
    
    /**
     * Return true if every monsters are removed
     */
    isEverythingRemoved() {
        return this.maxLength == this.nbMonstersRemoved;
    }
}