/**
 * Wave: contains all monsters on a level
 */
class Wave extends createjs.EventDispatcher {
    constructor(path) {
        super();
        
        this.graphics = new WaveGraphicsComponent();
        utils.MonsterFactory.currentPath = path;
        
        this._monsters = null;
        this.startTime = 0;
        this.tickListener = null;
        
        this.init();
    }
    
    /**
     * Send the finish event
     */
    _sendFinishEvent() {
        this.dispatchEvent(new createjs.Event("finish"));
    }
    
    /**
     * Create a monster with the given data
     */
    _createMonster(data) {
        let monster = utils.MonsterFactory.createMonster(data.name);
        
        monster.delay = data.delay;
        monster.on("die", this.handleMonsterDie, this);
        monster.on("survive", this.handleMonsterSurvive, this);
        
        this._monsters.add(monster);
    }
    
    /**
     * Remove a monster from the wave
     */
    _removeMonster(monster) {
        this.graphics.removeMonster(monster);
        this._monsters.remove(monster);
        
        if (this._monsters.isEverythingRemoved()) {
            this._monsters = null;
            this._sendFinishEvent();
        }
    }
    
    /**
     * Init the wave
     */
    init() {
        this.graphics.init();
    }
    
    /**
     * Return all wave monsters
     */
    get monsters() {
        return Object.values(this._monsters.monsters);
    }
    
    /**
     * Start the wave from the given wave info
     */
    startWave(waveInfo) {
        let logMessage = `Wave ${waveInfo.num}, ${waveInfo.monsters.length} monster(s)`;
        utils.logMethod(this, 'startWave', '', logMessage);
        
        // create monsters collection
        this._monsters = new MonsterCollection();
        for (let data of waveInfo.monsters)
            this._createMonster(data);
        
        this.tickListener = this.graphics.on('tick', this.handleTick, this);
        this.startTime = createjs.Ticker.getTime(true);
    }
    
    /**
     * Callback when a monster die
     */
    handleMonsterDie(event) {
        this._removeMonster(event.currentTarget);
    }
    
    /**
     * Callback when a monster survive
     */
    handleMonsterSurvive(event) {
        this._removeMonster(event.currentTarget);
    }
    
    /**
     * Callback when game tick
     */
    handleTick(event) {
        if (game.paused)
            return;
            
        // if all monsters are released, we are nothing left to do
        if (this._monsters.isEverythingReleased()) {
            utils.logMethod(this, 'handleTick', '', 'All monsters are released!');
            this.graphics.off('tick', this.tickListener);
            this.tickListener = null;
            return;
        }
        
        let time = createjs.Ticker.getTime(true);
        let delay = time - this.startTime;

        let monsters = this._monsters.releaseMonsters(delay/1000);
        for (let monster of monsters)
            this.graphics.addMonster(monster);
    }
}