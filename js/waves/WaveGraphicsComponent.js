/**
 * WaveGraphicsComponent: graphics component for waves
 */
class WaveGraphicsComponent extends createjs.Container {
    constructor() {
        super();
    }
    
    /**
     * Init the wave graphics component
     */
    init() {
        this.name = 'cntWaves';
    }
    
    /**
     * Update the wave graphics component
     */
    update() { }
    
    /**
     * Add the given monster to the wave
     */
    addMonster(monster) {
        this.addChild(monster.graphics);
    }
    
    /**
     * Remove the given monster to the wave
     */
    removeMonster(monster) {
        monster.graphics.removeAll();
        this.removeChild(monster.graphics);
    }
}